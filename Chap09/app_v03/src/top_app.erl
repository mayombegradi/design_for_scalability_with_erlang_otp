-module(top_app).
%%% ========================================================= %%%
-behaviour(application).
%%% ========================================================= %%%
-export([start/2]).
-export([start_phase/3]).

-export([stop/1]).
%%% ========================================================= %%%
%%% ===================== API Functions ===================== %%%
%%% ========================================================= %%%
-spec start(atom(), list()) -> {ok, pid()}.
start(_StartType, _StartArgs) ->
    {ok, _Pid} = bsc_sup:start_link().

-spec start_phase(atom(), normal | tuple(), term()) -> ok.
start_phase(StartPhase, StartType, Args) ->
    Msg = "top_app:start_phase(~p, ~p, ~p).~n",
    io:format(Msg, [StartPhase, StartType, Args]).

-spec stop(term()) -> no_return().
stop(_Data) ->
    ok.