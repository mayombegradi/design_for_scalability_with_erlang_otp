-module(loggers).
%%% ========================================================= %%%
-behaviour(gen_event).
%%% ========================================================= %%%
-export([init/1]).
-export([handle_call/2]).
-export([handle_event/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(standard_io) ->
    {ok, {standard_io, 1}};
init({file, File}) ->
    {ok, Pid} = file:open(File, write),
    {ok, {Pid, 1}};
init({standard_io, {Pid, Count}}) when is_pid(Pid) ->
    file:close(Pid),
    {ok, {standard_io, Count}};
init({{file, File}, {standard_io, Count}}) when is_list(File) ->
    {ok, Pid} = file:open(File, write),
    {ok, {Pid, Count}};
init(Args) ->
    {error, {args, Args}}.

handle_call(_Event, {Type, Count}) ->
    {ok, ok, {Type, Count+1}}.

handle_event(Event, {Type, Count}) ->
    print(Type, Count, Event, "Event"),
    {ok, {Type, Count+1}}.

handle_info(Event, {Type, Count}) ->
    print(Type, Count, Event, "Unknown"),
    {ok, {Type, Count+1}}.

terminate(swap, {Type, Count}) ->
    {Type, Count};
terminate(_Reason, {standard_io, Count}) ->
    {count, Count};
terminate(_Reason, {Pid, Count}) ->
    file:close(Pid),
    {count, Count}.

%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
print(Type, Count, Event, Tag) ->
    Msg = "Id: ~w Time: ~w Date: ~w~n" ++ Tag ++ ": ~w~n",
    io:format(Type, Msg, [Count, time(), date(), Event]).
