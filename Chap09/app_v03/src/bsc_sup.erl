-module(bsc_sup).
%%% ========================================================= %%%
-behaviour(supervisor).
%%% ========================================================= %%%
-export([start_link/0]).

-export([init/1]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(_Args) ->
    Children = [{freq_overload, worker},
                {frequency, worker},
                {simple_phone_sup, supervisor}],

    ChildSpecList = [child(Mod, Type) || {Mod, Type} <- Children],
    SupFlag = #{ strategy => rest_for_one,
                 intensity => 2,
                 period => 3600 },
    {ok, {SupFlag, ChildSpecList}}.

%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
child(Module, Type) ->
    #{ id => Module,
       start => {Module, start_link, []},
       restart => permanent,
       shutdown => 2000,
       type => Type,
       modules => [Module] }.
