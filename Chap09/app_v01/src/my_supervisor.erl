-module(my_supervisor).
%%% ========================================================= %%%
-export([start/2]).
-export([stop/1]).

-export([init/1]).
%%% ========================================================= %%%
-type name() :: string().
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start(name(), [mfa()]) -> {ok, pid()}.
start(Name, ChildSpecList) ->
    register(Name, Pid = spawn(?MODULE, init, [ChildSpecList])),
    {ok, Pid}.

-spec stop(name()) -> no_return().
stop(Name) -> Name ! stop.

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(ChildSpecList) ->
    process_flag(trap_exit, true),
    loop(start_children(ChildSpecList)).
%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
loop(ChildList) ->
    receive
        {'EXIT', Pid, normal} ->
            loop(lists:keydelete(Pid, 1, ChildList));
        {'EXIT', Pid, _Reason} ->
            NewChildList = restart_child(Pid, ChildList),
            loop(NewChildList);
        stop ->
            terminate(ChildList)
    end.

start_children(ChildSpecList) ->
    [{element(2, apply(Mod, Func, Args)), {Mod, Func, Args}} ||
     {Mod, Func, Args} <- ChildSpecList].

restart_child(Pid, ChildList) ->
    {Pid, {Mod, Func, Args}} = lists:keyfind(Pid, 1, ChildList),
    {ok, NewPid} = apply(Mod, Func, Args),
    lists:keyreplace(Pid, 1, ChildList, {NewPid, {Mod, Func, Args}}).

terminate(ChildList) ->
    lists:foreach(fun({Pid, _MFA}) -> exit(Pid, kill) end, ChildList).