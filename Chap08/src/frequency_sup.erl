-module(frequency_sup).
%%% ========================================================= %%%
-behaviour(supervisor).
%%% ========================================================= %%%
-export([start_link/0]).
-export([stop/0]).

-export([init/1]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

-spec stop() -> no_return().
stop() ->
    exit(whereis(?MODULE), shutdown).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(_Args) ->
    SupFlag = #{ strategy => rest_for_one,
                 intensity => 2,
                 period => 3600 },
    ChildSpecList = [child(freq_overload), child(frequency)],
    {ok, {SupFlag, ChildSpecList}}.

%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
child(Module) ->
    #{ id => Module,
       start => {Module, start_link, []},
       restart => permanent,
       shutdown => 2000,
       type => worker,
       modules => [Module] }.