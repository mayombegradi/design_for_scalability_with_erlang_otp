-module(hw).
%%% ========================================================= %%%
-vsn('1.0').
%%% ========================================================= %%%
-export([display/2]).
-export([return_change/1]).
-export([drop_cup/0]).
-export([prepare/1]).
-export([reboot/0]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec display(string(), list()) -> no_return().
display(Msg, Arg) -> io:format("Display: " ++ Msg ++ "~n", Arg).

-spec return_change(integer()) -> no_return().
return_change(Coin) -> io:format("Machine: Returned ~w in change~n", [Coin]).

-spec drop_cup() -> no_return().
drop_cup() -> io:format("Machine: Dropped cup.~n").

-spec prepare(atom()) -> no_return().
prepare(Type) -> io:format("Machine: Preparing ~p.~n", [Type]).

-spec reboot() -> no_return().
reboot() -> io:format("Machine: Rebooted Hardware~n").
