-module(coffee_sup).
%%% ========================================================= %%%
-vsn('1.0').
%%% ========================================================= %%%
-behaviour(supervisor).
%%% ========================================================= %%%
-export([start_link/0]).

-export([init/1]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init([]) ->
    SupFlags = #{ strategy => one_for_one,
                  intensity => 5,
                  period => 10 },
    ChildSpec = #{ id => coffee_gen_statem,
                   start => {coffee_gen_statem, start_link, []},
                   restart => permanent,
                   shutdown => 5000,
                   type => worker,
                   modules => [coffee_gen_statem] },
    {ok, {SupFlags, [ChildSpec]}}.