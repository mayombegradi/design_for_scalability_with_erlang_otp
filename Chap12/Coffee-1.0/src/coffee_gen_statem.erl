-module(coffee_gen_statem).
%%% ========================================================= %%%
-vsn('1.0').
%%% ========================================================= %%%
-behaviour(gen_statem).
%%% ========================================================= %%%
-export([start_link/0]).
-export([stop/0]).

-export([tea/0]).
-export([espresso/0]).
-export([americano/0]).
-export([cappuccino/0]).

-export([cup_removed/0]).
-export([pay/1]).
-export([cancel/0]).

-export([init/1]).
-export([callback_mode/0]).
-export([terminate/3]).

-export([selection/3]).
-export([payment/3]).
-export([remove/3]).
%%% ========================================================= %%%
-record(data, { type     :: atom(),
                price    :: integer(),
                paid = 0 :: integer() }).
%%% ========================================================= %%%
-define(TIMEOUT, 10000).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_statem:start_link({local, ?MODULE}, ?MODULE, [], []).

-spec stop() -> no_return().
stop() ->
    gen_statem:stop(?MODULE).

%% Client Functions for Drink Selections
-spec tea() -> no_return().
tea() -> gen_statem:cast(?MODULE, {selection, ?FUNCTION_NAME, 100}).

-spec espresso() -> no_return().
espresso()   -> gen_statem:cast(?MODULE, {selection, ?FUNCTION_NAME, 150}).

-spec americano() -> no_return().
americano()  -> gen_statem:cast(?MODULE, {selection, ?FUNCTION_NAME, 100}).

-spec cappuccino() -> no_return().
cappuccino() -> gen_statem:cast(?MODULE, {selection, ?FUNCTION_NAME, 150}).

%% Client Functions for Actions
-spec cup_removed() -> no_return().
cup_removed() -> gen_statem:cast(?MODULE, ?FUNCTION_NAME).

-spec pay(integer()) -> no_return().
pay(Coin) -> gen_statem:cast(?MODULE, {?FUNCTION_NAME, Coin}).

-spec cancel() -> no_return().
cancel() -> gen_statem:cast(?MODULE, ?FUNCTION_NAME).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init([]) ->
    process_flag(trap_exit, true),

    hw:reboot(),
    hw:display("Make Your Selection", []),
    {ok, selection, #data{}}.

callback_mode() -> state_functions.

terminate(_Reason, _State, #data{ paid=Paid }) when Paid > 0 ->
    hw:return_change(Paid), ok;
terminate(_Reason, _State, #data{}) ->
    ok.

%%% ========================================================= %%%
%%% =================== State Functions ===================== %%%
%%% ========================================================= %%%
selection(cast, {selection, Type, Price}, Data=#data{}) ->
    hw:display("Please pay: ~w", [Price]),
    {next_state, payment, Data#data{ type=Type, price=Price }, ?TIMEOUT};
selection(cast, {pay, Coin}, #data{}) ->
    hw:return_change(Coin),
    keep_state_and_data;
selection(_EventType, _EventContent, _Data) ->
    keep_state_and_data.

payment(cast, {pay, Coin}, #data{ paid=Paid,
                                  price=Price,
                                  type=Type }) when Coin+Paid >= Price ->
    hw:display("Preparing Drink", []),
    hw:return_change(Coin+Paid - Price),
    hw:drop_cup(), hw:prepare(Type),
    hw:display("Remove Drink.", []),
    {next_state, remove, #data{}};
payment(cast, {pay, Coin}, Data=#data{ paid=Paid, price=Price }) ->
    ToPay = Price - (Coin+Paid),
    hw:display("Please pay: ~w", [ToPay]),
    {keep_state, Data#data{ paid=Coin+Paid }, ?TIMEOUT};
payment(cast, cancel, #data{ paid=Paid }) ->
    hw:display("Make Your Selection", []),
    hw:return_change(Paid),
    {next_state, selection, #data{}};
payment(timeout, ?TIMEOUT, #data{ paid=Paid }) ->
    hw:display("Make Your Selection", []),
    hw:return_change(Paid),
    {next_state, selection, #data{}};
payment(_EventType, _EventContent, _Data) ->
    keep_state_and_data.

remove(cast, cup_removed, _Data) ->
    hw:display("Make Your Selection", []),
    {next_state, selection, #data{}};
remove(cast, {pay, Coin}, _Data) ->
    hw:return_change(Coin),
    keep_state_and_data;
remove(_EventType, _EventContent, _Data) ->
    keep_state_and_data.
