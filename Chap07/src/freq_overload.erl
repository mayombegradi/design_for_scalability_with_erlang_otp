-module(freq_overload).
%%% ========================================================= %%%
-export([start_link/0]).
-export([no_frequency/0]).
-export([frequency_available/0]).
-export([frequency_denied/0]).
-export([add/2]).
-export([delete/2]).
%%% ========================================================= %%%
-type handler() :: atom().
-type args() :: term().
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()} | term().
start_link() ->
    case gen_event:start_link({local, ?MODULE}) of
        {ok, Pid} ->
            add(counter, null),
            add(loggers, {file, "log"}),
            {ok, Pid};
        Error ->
            Error
    end.

-spec no_frequency() -> no_return().
no_frequency() ->
    gen_event:notify(?MODULE, {set_alarm, {no_frequency, self()}}).

-spec frequency_available() -> no_return().
frequency_available() ->
    gen_event:notify(?MODULE, {clear_alarm, no_frequency}).

-spec frequency_denied() -> no_return().
frequency_denied() ->
    gen_event:notify(?MODULE, {event, {frequency_denied, self()}}).

-spec add(handler(), args()) -> no_return().
add(Handler, Args) ->
    gen_event:add_sup_handler(?MODULE, Handler, Args).

-spec delete(handler(), args()) -> no_return().
delete(Handler, Args) ->
    gen_event:delete_handler(?MODULE, Handler, Args).