-module(tcp_print).
-behaviour(tcp_wrapper).
-export([init_request/0]).
-export([get_request/2]).
-export([stop_request/2]).

init_request() ->
    io:format("Receiving Data:~n."),
    {ok, []}.

get_request(Data, Buffer) ->
    io:format("."),
    {ok, [Data|Buffer]}.

stop_request(_Reason, Buffer) ->
    io:format("~n"),
    io:format(lists:reverse(Buffer)),
    io:format("~n").