-module(tracer_demo).
%%% ========================================================= %%%
-export([func/3]).
%%% ========================================================= %%%
-type func_state() :: integer().
-type event() :: any().
-type opaque() :: term().
%%% ========================================================= %%%

% https://www.erlang.org/doc/man/sys.html#install-2
-spec func(func_state(), event(), opaque()) -> func_state().
func(FuncState, {out, {error, no_frequency}, From, _State}, _ProcState) ->
    Message = "*DBG* Warning, Client ~p refused frequency! Count: ~w~n",
    io:format(Message, [From, FuncState]),
    FuncState+1;
func(FuncState, _Event, _ProcState) ->
    FuncState.