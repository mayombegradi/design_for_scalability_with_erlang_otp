-module(coffee).
%%% ========================================================= %%%
-export([start_link/0]).
-export([tea/0]).
-export([espresso/0]).
-export([americano/0]).
-export([cappuccino/0]).

-export([cup_removed/0]).
-export([pay/1]).
-export([cancel/0]).

-export([init/0]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    {ok, spawn_link(?MODULE, init, [])}.

%% Client Functions for Drink Selections
-spec tea() -> no_return().
tea() -> ?MODULE ! {selection, ?FUNCTION_NAME, 100}.

-spec espresso() -> no_return().
espresso()   -> ?MODULE ! {selection, ?FUNCTION_NAME, 150}.

-spec americano() -> no_return().
americano()  -> ?MODULE ! {selection, ?FUNCTION_NAME, 100}.

-spec cappuccino() -> no_return().
cappuccino() -> ?MODULE ! {selection, ?FUNCTION_NAME, 150}.

%% Client Functions for Actions
-spec cup_removed() -> no_return().
cup_removed() -> ?MODULE ! ?FUNCTION_NAME.

-spec pay(integer()) -> no_return().
pay(Coin) -> ?MODULE ! {?FUNCTION_NAME, Coin}.

-spec cancel() -> no_return().
cancel() -> ?MODULE ! ?FUNCTION_NAME.

init() ->
    register(?MODULE, self()),
    hw:reboot(),
    hw:display("Make Your Selection", []),
    selection().

%%% ========================================================= %%%
%%% ================== State Functions ====================== %%%
%%% ========================================================= %%%
selection() ->
    receive
        {selection, Type, Price} ->
            hw:display("Please pay: ~w", [Price]),
            payment(Type, Price, 0);
        {pay, Coin} ->
            hw:return_change(Coin),
            selection();
        _Other ->
            selection()
    end.

payment(Type, Price, Paid) ->
    receive
        {pay, Coin} ->
            case Coin+Paid >= Price of
                true ->
                    hw:display("Preparing Drink", []),
                    hw:return_change(Coin+Paid - Price),
                    hw:drop_cup(), hw:prepare(Type),
                    hw:display("Remove Drink.", []),
                    remove();
                false ->
                    ToPay = Price - (Coin+Paid),
                    hw:display("Please pay: ~w", [ToPay]),
                    payment(Type, Price, Coin+Paid)
            end;
        cancel ->
            hw:display("Make Your Selection", []),
            hw:return_change(Paid),
            selection();
        _Other ->
            payment(Type, Price, Paid)
    end.

remove() ->
    receive
        cup_removed ->
            hw:display("Make Your Selection", []),
            selection();
        {pay, Coin} ->
            hw:return_change(Coin),
            remove();
        _Other ->
            remove()
    end.
