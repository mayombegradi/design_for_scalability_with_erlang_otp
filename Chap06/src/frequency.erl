-module(frequency).
%%% ========================================================= %%%
-behaviour(gen_server).
%%% ========================================================= %%%
-export([start/0]).
-export([start_link/0]).
-export([stop/0]).

-export([allocate/0]).
-export([deallocate/1]).

-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
-type frequency() :: integer().
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start() -> {ok, pid()}.
start() ->
    gen_server:start({local, ?MODULE}, ?MODULE, [], []).

-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

-spec stop() -> no_return().
stop() ->
    gen_server:cast(?MODULE, stop).

-spec allocate() -> {ok, frequency()} | {error, no_frequency}.
allocate() ->
    gen_server:call(?MODULE, {allocate, self()}).

-spec deallocate(frequency()) -> no_return().
deallocate(Frequency) ->
    gen_server:cast(?MODULE, {deallocate, Frequency}).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(_Args) ->
    erlang:process_flag(trap_exit, true),
    Frequencies = {lists:seq(10, 60), []},
    {ok, Frequencies}.

handle_call({allocate, Pid}, _From, Frequencies) ->
    {NewFrequencies, Reply} = allocate(Frequencies, Pid),
    {reply, Reply, NewFrequencies}.

handle_cast({deallocate, Frequency}, Frequencies) ->
    NewFrequencies = deallocate(Frequencies, Frequency),
    {noreply, NewFrequencies};
handle_cast(stop, Frequencies) ->
    {stop, normal, Frequencies}.

handle_info(_Msg, Frequencies) ->
    {noreply, Frequencies}.

terminate(_Reason, _Frequencies) ->
    ok.

%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
allocate({[], Allocated}, _Pid) ->
    {{[], Allocated}, {error, no_frequency}};
allocate({[Freq|Free], Allocated}, Pid) ->
    {{Free, [{Freq, Pid}|Allocated]}, {ok, Freq}}.

deallocate({Free, Allocated}, Freq) ->
    NewAllocated = lists:keydelete(Freq, 1, Allocated),
    {[Freq|Free], NewAllocated}.