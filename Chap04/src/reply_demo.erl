-module(reply_demo).
%%% ========================================================= %%%
-behaviour(gen_server).
%%% ========================================================= %%%
-export([start/0]).
-export([add/1]).

-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start() -> {ok, pid()}.
start() ->
    gen_server:start({local, ?MODULE}, ?MODULE, 0, []).

-spec add(number()) -> ok.
add(Number) ->
    gen_server:call(?MODULE, {add, Number}).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(Args) ->
    {ok, Args}.

handle_call({add, Data}, From, Sum) ->
    gen_server:reply(From, ok),
    timer:sleep(1000),
    NewSum = Sum+Data,
    io:format("From: ~p, Sum:~p~n", [From, NewSum]),
    {noreply, NewSum}.

handle_cast(_Msg, Sum) ->
    {noreply, Sum}.

handle_info(_Msg, Sum) ->
    {noreply, Sum}.

terminate(_Reason, _Sum) ->
    ok.