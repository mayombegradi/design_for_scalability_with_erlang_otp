-module(timeout).
%%% ========================================================= %%%
-behaviour(gen_server).
%%% ========================================================= %%%
-export([start_link/0]).
-export([sleep/1]).

-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

-spec sleep(integer()) -> ok | no_return().
sleep(Millis) ->
    gen_server:call(?MODULE, {sleep, Millis}).

%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(Args) ->
    process_flag(trap_exit, true),
    {ok, Args}.

handle_call({sleep, Millis}, _From, State) ->
    timer:sleep(Millis),
    {reply, yes, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Msg, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.