-module(ping).
%%% ========================================================= %%%
-behaviour(gen_server).
%%% ========================================================= %%%
-export([start_link/0]).
-export([start_ping/0]).
-export([pause_ping/0]).

-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
-define(TIMEOUT, 5000).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

-spec start_ping() -> started.
start_ping() ->
    gen_server:call(?MODULE, start).

-spec pause_ping() -> paused.
pause_ping() ->
    gen_server:call(?MODULE, pause).

%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(_Args) ->
    process_flag(trap_exit, true),
    {ok, undefined, ?TIMEOUT}.

handle_call(start, _From, State) ->
    {reply, started, State, ?TIMEOUT};
handle_call(pause, _From, State) ->
    {reply, paused, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(timeout, State) ->
    {_Hour, _Min, Sec} = time(),
    io:format("~2.w~n", [Sec]),
    {noreply, State, ?TIMEOUT};
handle_info(_Msg, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.